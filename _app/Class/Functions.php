<?php

namespace Classes;

class Functions {

    static public function chk_array($array, $key) {
        if (isset($array[$key]) && !empty($array[$key])) {
            return $array[$key];
        }
    }

    static public function load_model($model = false) {
        if (!$model) {
            return false;
        } else {
            $model = '\Models\\' . ucfirst($model);
            if (class_exists($model)) {
                return new $model();
            } else {
                echo "Model {$model} inexistente!";
                return false;
            }
        }
    }

}
