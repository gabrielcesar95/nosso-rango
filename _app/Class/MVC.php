<?php

/**
 * MVC.class [ HELPER ]
 * Classe responsável pelo gerenciamento da estrutura MVC
 * @copyright (c) 2018, Gabriel Cesar Mello
 */
namespace Classes;

class MVC {

    private $Controller;
    private $Action;
    private $Parameters;

    function __construct() {
        $this->get_url_data();
                
        if (!$this->Controller) {
            $this->Controller = new \Controllers\Home();

            $this->Controller->index();

            return;
        }

        //Formatar nome da classe controller antes de instanciar
        $this->Controller = ucfirst($this->Controller);


        // Se o arquivo do controller não existir, instanciar classe home com o método de not found
        if (!class_exists($this->Controller)) {
            $this->Controller = new \Controllers\Home();
            $this->Controller->not_found();

            return;
        }

        // Instanciar controller solicitado, passando os parâmetros
        $this->Controller = new $this->Controller($this->Parameters);

        // Se o método indicado existir, executa o método e envia os parâmetros
        if (method_exists($this->Controller, $this->Action)) {
            
            $this->Controller->{$this->Action}($this->Parameters);

            return;
        }

        // Sem ação, chamar o método index
        if (!$this->Action && method_exists($this->Controller, 'index')) {
            $this->Controller->index($this->Parameters);

            // FIM :)
            return;
        }
        
        //Caso o método index não exista, chamar método not_found
        $this->Controller = new \Controllers\Home();
        $this->Controller->not_found();
        
        
        
        return;
    }

    /**
     * Get information from URL (GET)
     *
     * Get parameters from URL and set them on $this->Controller, $this->Action and $this->Parameters
     *
     * The URL must have this shape:
     * http://www.example.com/controller/action/parameter1/parameter2/etc...
     */
    public function get_url_data() {
        if (filter_input(INPUT_GET, 'path')) {
            $path = filter_input(INPUT_GET, 'path');

            $path = rtrim($path, '/');
            $path = filter_var($path, FILTER_SANITIZE_URL);

            $path = explode('/', $path);

            $this->Controller = '\\Controllers\\' . ucfirst(chk_array($path, 0));
            $this->Action = Functions::chk_array($path, 1);
            

            if (chk_array($path, 2)) {
                unset($path[0]);
                unset($path[1]);

                $this->Parameters = array_values($path);
            }
        }
    }

}
