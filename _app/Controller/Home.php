<?php

/**
 * home.controller [ CONTROLLER ]
 * CONTROLLER BASE DO SISTEMA
 * @copyright (c) 2018, Gabriel Cesar Mello
 */

namespace Controllers;

class Home extends \Classes\MainController{
    function __construct() {
        
    }
    
    public function index() {
        $parametros = ( func_num_args() >= 1 ) ? func_get_arg(0) : array();
        
        $model = \Classes\Functions::load_model('home');
        $model->listarTelefones();
        
        
    }
    
    public function not_found() {
        echo 'Erro 404';
    }

}
