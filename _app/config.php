<?php

// CAMINHOS ##################
define('BASE_PATH', dirname(__FILE__));
define('UPLOAD_PATH', BASE_PATH . '/upload');
define('HOME', 'http://localhost/nossorango');

// CONFIGRAÇÕES DO BANCO ####################
const DATABASE = [
    'HOST' => 'localhost',
    'PORT' => '3306',
    'USER' => 'root',
    'PASS' => '',
    'BASE' => 'nossorango'
];

define('MAIL_USER', 'example@mail.com');
define('MAIL_PASS', 'SENHA AQUI');
define('MAIL_PORT', '587');
define('MAIL_HOST', 'smtp.example.com');

// DEFINE IDENTIDADE DO SITE ###############
define('SITENAME', 'Nosso Rango');
define('SITEDESC', 'Descrição do Site');

define('DEBUG', true);

// TRATAMENTO DE ERROS #####################
//CSS constantes :: Mensagens de Erro
define('WS_ACCEPT', 'accept');
define('WS_INFOR', 'infor');
define('WS_ALERT', 'alert');
define('WS_ERROR', 'error');

//WSErro :: Exibe erros lançados :: Front
function error($ErrMsg, $ErrNo, $ErrDie = null) {
    $CssClass = ($ErrNo == E_USER_NOTICE ? WS_INFOR : ($ErrNo == E_USER_WARNING ? WS_ALERT : ($ErrNo == E_USER_ERROR ? WS_ERROR : $ErrNo)));
    echo "<p class=\"trigger {$CssClass}\">{$ErrMsg}<span class=\"ajax_close\"></span></p>";

    if ($ErrDie):
        die;
    endif;
}

//PHPErro :: personaliza o gatilho do PHP
function fullError($ErrNo, $ErrMsg, $ErrFile, $ErrLine) {
    $CssClass = ($ErrNo == E_USER_NOTICE ? WS_INFOR : ($ErrNo == E_USER_WARNING ? WS_ALERT : ($ErrNo == E_USER_ERROR ? WS_ERROR : $ErrNo)));
    echo "<p class=\"trigger {$CssClass}\">";
    echo "<b>Erro na Linha: #{$ErrLine} ::</b> {$ErrMsg}<br>";
    echo "<small>{$ErrFile}</small>";
    echo "<span class=\"ajax_close\"></span></p>";

    if ($ErrNo == E_USER_ERROR):
        die;
    endif;
}

set_error_handler('fullError');
