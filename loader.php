<?php
require_once __DIR__ . '/ext_lib/autoload.php';
//require_once __DIR__ . '/_app/functions.php';
require_once __DIR__ . '/_app/config.php';

if (!defined('BASE_PATH')) {
    exit;
}

if (!defined('DEBUG') || DEBUG == false) {
    error_reporting(0);
    ini_set('display_errors', 0);
} else {
    error_reporting(E_ALL);
    ini_set('display_errors', 1);
}


$MVC = new \Classes\MVC();